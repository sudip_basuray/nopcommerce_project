package com.nopcommerce.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserFactory
{
	
	static WebDriver driver;
	
	public static WebDriver StartWebBrowser(String browsername, String url)
	{
		if(browsername.equals("firefox"))
		{
			driver =new FirefoxDriver();
			
		}
		else if(browsername.equals("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver=new ChromeDriver();
			
		}
		else
		{
			System.out.println("no browser found");
		}
		driver.manage().window().maximize();
		driver.get(url);
		
		return driver;
	}
	
}
