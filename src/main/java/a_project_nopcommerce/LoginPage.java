package a_project_nopcommerce;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.nopcommerce.utility.WaitElement;



public class LoginPage
{
	WebDriver driver;
	WaitElement waitelemtn;
	/*
	 * object identification
	 */
	@FindBy(xpath="//input[@type='email']")WebElement userName;
	@FindBy(xpath="//input[@type='password']")WebElement password;
	@FindBy(xpath="//input[@type='submit']")WebElement loginbtn;
	
	/*
	 * constructor create
	 */
	public LoginPage(WebDriver driver)
	{
		this.driver= driver;
	}
	
	/*
	 * entering username
	 */
	public void usernameEntry(String usernameforapplication)
	{
		WaitElement.WaitForElement(userName, 30, driver);
		userName.sendKeys(usernameforapplication);
	}
	
	public void passwordentry(String passwordforapplication)
	{
		WaitElement.WaitForElement(password, 30, driver);
		password.sendKeys(passwordforapplication);
	}
	
	public void loginbuttonclick()
	{
		WaitElement.WaitForElement(loginbtn, 40, driver);
		loginbtn.click();
		WaitElement.waitTill(3000);
		
	}
	
	public boolean verifyvalidlogin()
	{
		String exptitle = "Dashboard / nopCommerce administration";
		String acttitle =driver.getTitle();
		if(exptitle.equals(acttitle))
		{
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean verifyinvalidlogin()
	{
		String exptitle ="https://admin-demo.nopcommerce.com/admin/";
		String acttitle =driver.getTitle();
		if(exptitle!=acttitle)
		{
			return true;
		}
		else {
			return false;
		}
	}
}
